package com.multunus.test;

public interface Constants {

	public static final String TITLE_OF_FIRST_ARTICLE = "Wash your sins online at Saranam.com";
	static final String TITLE_OF_LAST_ARTICLE = "'I unlock Joy' program for developers & students - Windows Phones up for grabs";
	public static final int NUMBER_OF_ITEMS = 10;

}
