package com.multunus.test;


import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.Smoke;
import android.widget.TextView;

import com.jayway.android.robotium.solo.Solo;
import com.multunus.FeedItem;
import com.multunus.InformationActivity;
import com.multunus.ListOfTitlesActivity;
import com.multunus.R;
import com.multunus.ShortenURL;
import com.multunus.ShowFeedDetailsActivity;

public class ListActivityTest extends
		ActivityInstrumentationTestCase2<ListOfTitlesActivity> {

	private Solo solo;

	public ListActivityTest() {
		super("com.multunus.test", ListOfTitlesActivity.class);

	}

	@Override
	public void setUp() throws Exception {
		solo = new Solo(getInstrumentation(), getActivity());		
	}

	@Override
	protected void tearDown() throws Exception {
		solo.finishOpenedActivities();
	}

	@Smoke
	public void testListing() throws Exception {		
		solo.assertCurrentActivity("", ListOfTitlesActivity.class);	
		solo.sleep(500);
		assertEquals(Constants.NUMBER_OF_ITEMS, getActivity().getListAdapter()
				.getCount());		
	}

	@Smoke
	public void testItemClick() throws Exception {
		solo.sleep(500);
		solo.clickInList(0);
		String firstTitle = ((FeedItem) getActivity().getListAdapter().getItem(
				0)).getTitle();		
		assertEquals(firstTitle, ((TextView) solo.getView(R.id.storyTitle))
				.getText());
		solo.assertCurrentActivity("", ShowFeedDetailsActivity.class);
		assertEquals(solo.getCurrentActivity().findViewById(R.id.shareButton),solo.getButton(0));
		solo.goBack();
		solo.scrollDown();
	}
	
	@Smoke
	public void testOrientation() throws InterruptedException {
		solo.sleep(500);
		solo.setActivityOrientation(Solo.LANDSCAPE);
		assertNotNull(getActivity().getListAdapter());
	}

	@Smoke
	public void testShareButtonClick() throws Exception{
		solo.sleep(500);
		ListOfTitlesActivity currentActivity = getActivity();
		FeedItem feedItem = (FeedItem) currentActivity.getListAdapter()
		.getItem(0);
		solo.clickInList(0);
		solo.clickOnButton(0);
		String url = new ShortenURL(currentActivity
				.getString(R.string.bitly_login), currentActivity
				.getString(R.string.bitly_api_key)).getShortUrl(feedItem
				.getContentURL());
		solo.sleep(100);
		Intent shareIntent = (Intent) solo.getCurrentActivity().getIntent()
				.getExtras().get(Intent.EXTRA_INTENT);
		assertEquals(currentActivity.getString(R.string.share_message_type),
				shareIntent.getType());
		assertEquals(Intent.ACTION_SEND, shareIntent.getAction());
		assertEquals(feedItem.getTitle(), shareIntent
				.getStringExtra(Intent.EXTRA_SUBJECT));
		assertTrue(shareIntent.getStringExtra(Intent.EXTRA_TEXT).contains(
				currentActivity.getString(R.string.share_message)));
		assertTrue(shareIntent.getStringExtra(Intent.EXTRA_TEXT).contains(url));
	}
	
	@Smoke
	public void testRefresh(){
		solo.sleep(500);
		ListOfTitlesActivity currentActivity = getActivity();
		solo.scrollDown();		
		solo.scrollUp();
		String refreshMsg = currentActivity.getString(R.string.pull_to_refresh_tap_label);
		assertTrue(solo.searchText(refreshMsg));
	}
	
	@Smoke
	public void testMenu(){
		solo.sleep(500);
		solo.clickOnMenuItem(getActivity().getString(R.string.info_menu_title));
		assertEquals(InformationActivity.class,solo.getCurrentActivity().getClass());
	}
}
