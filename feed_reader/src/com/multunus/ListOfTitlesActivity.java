package com.multunus;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.airbrake.AirbrakeNotifier;
import com.markupartist.android.widget.PullToRefreshListView;
import com.markupartist.android.widget.PullToRefreshListView.OnRefreshListener;
import com.markupartist.android.widget.pulltorefresh.R;

public class ListOfTitlesActivity extends ListActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		new FetchFeed().execute();
		
        // Set a listener to be invoked when the list should be refreshed.
        ((PullToRefreshListView) getListView()).setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Do work to refresh the list here.
            	new FetchFeed().execute();
            }
        });
        AirbrakeNotifier.register(this, "4377dd3d46e51f7fa3573008109c764e");
	}
	
	public void onConfigurationChanged(Configuration newConfig) {		
	    super.onConfigurationChanged(newConfig);
	    Log.d(this.getLocalClassName(), "inside onConfigurationChanged");	    
	}

	@Override
	protected void onListItemClick(ListView listView, View view, int position,
			long id) {
		super.onListItemClick(listView, view, position, id);
		FeedItem item = (FeedItem) this.getListAdapter().getItem(position - 1);
		Intent showDetailsIntent = new Intent(this.getApplicationContext(),
				ShowFeedDetailsActivity.class);
		showDetailsIntent.putExtra("title", item.getTitle());
		showDetailsIntent.putExtra("content", item.getContent());
		showDetailsIntent.putExtra("contentURL", item.getContentURL());
		startActivity(showDetailsIntent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.home_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.infoIcon:
			Intent intent = new Intent(getApplicationContext(),
					InformationActivity.class);
			startActivity(intent);
			break;
		}
		return true;
	}

	private class FeedDataAdapter extends BaseAdapter {

		private Context activity;
		private List<FeedItem> feedItems;

		FeedDataAdapter(Activity activity, List<FeedItem> feedItems) {
			this.activity = activity;
			this.feedItems = feedItems;
		}

		@Override
		public int getCount() {
			return feedItems.size();
		}

		@Override
		public FeedItem getItem(int position) {
			return feedItems.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View rowView = convertView;
			TitleView titleView = null;
			if (rowView == null) {
				LayoutInflater inflater = ((Activity) activity)
						.getLayoutInflater();
				rowView = inflater.inflate(R.layout.row, null);
				titleView = new TitleView();
				titleView.title = (TextView) rowView
						.findViewById(R.id.title_row);
				rowView.setTag(titleView);
			} else {
				titleView = (TitleView) rowView.getTag();
			}

			FeedItem currentFeedItem = feedItems.get(position);
			titleView.title.setText(currentFeedItem.getTitle());

			return rowView;
		}

	}

	protected static class TitleView {
		protected TextView title;
	}
	
	protected class FetchFeed extends AsyncTask<Void, Void, List<FeedItem>>{
		private ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {			
			progressDialog = new ProgressDialog(ListOfTitlesActivity.this);
			progressDialog.setMessage(getString(R.string.loading_msg));
			progressDialog.setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					cancel(false);
				}
			});
			try{
				progressDialog.show();
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
		}

		@Override
		protected List<FeedItem> doInBackground(Void... params){
			List<String> titles = new ArrayList<String>();
			List<FeedItem> feedItems = new ArrayList<FeedItem>();			

			try {
				RSSParser rssParser = new RSSParser(getString(R.string.feed_url));
				feedItems = rssParser.getListOfItemsFromFeed();
				for (FeedItem feedItem : feedItems) {
					titles.add(feedItem.getTitle());
				}
			} catch (Exception exception) {
				Log.w(ListOfTitlesActivity.class.getName(), "Failed to parse the feed",exception);
			}
			return feedItems;
		}

		@Override
		protected void onPostExecute(final List<FeedItem> feedItems) {
			super.onPostExecute(feedItems);
			ListOfTitlesActivity.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					try{
						FeedDataAdapter feedDataAdapter = new FeedDataAdapter(ListOfTitlesActivity.this, feedItems);
						setListAdapter(feedDataAdapter);
			            ((PullToRefreshListView) getListView()).onRefreshComplete();									
					}
					catch(Exception ex){
						Log.w(ListOfTitlesActivity.class.getName(), "Failed to set adapter",ex);
					}
					
				}
			});
			progressDialog.dismiss();
		}
		
	}
}