require 'sinatra'
require 'haml'
require 'rack/logger'

enable :logging
logger = Logger.new('app.log')
use Rack::CommonLogger, logger

before do
  logger.info "Processing #{request.path_info} #{request.request_method}"
  logger.info "Parameters #{params}"
  logger.info "#{request.env}"
end

get '/' do
  haml :index
end

post '/create' do
  tmp_dir = "tmp"
  app_name = "#{params[:app_name]}"
  @package_name = app_name.gsub(/\W+/, '').downcase
  app_dir = "#{tmp_dir}/#{@package_name}"
  Dir.mkdir("#{tmp_dir}") unless File.directory?(tmp_dir)
  Dir.mkdir("#{app_dir}") unless File.directory?(app_dir)
  system("rm -fr #{app_dir}/*")
  system("cp -R ../feed_reader/* #{app_dir}")
  system("cp -R ../pulltorefresh #{tmp_dir}")
  logo_file = params[:logo][:tempfile]
  File.open(File.join("#{app_dir}/res/drawable", "feeder_logo.png"), 'wb') {|f| f.write logo_file.read }
  system("cd #{app_dir} && ant -Dapp.name=\"#{app_name}\" -Dpackage.name=\"#{@package_name}\" -Dfeed.url=\"#{params[:url]}\" clean app-gen release >> ant.log ")
  haml :download
end

get '/download' do
  package_name = params[:package_name]
  file = File.join("tmp/#{package_name}/bin/","FeedParser-release.apk")
  send_file(file, :disposition => 'attachment', :filename => "#{package_name}.apk")
end

after do
  logger.info "Completed #{request.path_info } with #{response.status}"
end

